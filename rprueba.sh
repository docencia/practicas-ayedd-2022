#!/usr/bin/bash

addall() {
    git add 'target/surefire-reports/*.txt'
    git add '*.java'
    git add '*LEEME'
    git add '*.pdf'
    git add '*.txt'
    git add '*.PDF'
    git add '*.TXT'

    # rankings
    git add 'time.txt'
}

if [ "$USER" = paurea ]; then
	echo  do not run as $USER it is dangerous 1>&2
	exit 1
fi

{
    echo "before merge"
    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno

    git checkout rama-alumno
    git merge master
    echo "after merge"
} > ./rprueba.output 2>&1


if [[ "$1" = "-h" ]] || [[ $# -ne 2 ]];
then
    echo "Tests disponibles:"
    cat tests | sed 's/^/    /'
    echo ""
    echo "Tienes que introducir entrecomillados el nombre de los tests y un comentario:"
    echo "Ejemplo:"
    echo "    ./rprueba.sh \"ayedd.e1.TestMyEcho\" \"Arreglado código con argumentos\" "
    exit 1
fi


#
# set TESTS variable in .gitlab-ci.yml pipeline spec
#
cp .gitlab-ci.yml.orig .gitlab-ci.yml
sed -z "s/variables:\n/variables:\n  TESTS: \"$1\"\n/" .gitlab-ci.yml.orig  > .gitlab-ci.yml


#
# name of the exercise we want to test
#
EXERCISE=`echo $1 | sed "s/ayedd\.\(.*\)\..*/\1/"`

if [ "$EXERCISE" = c1 ]; then
	{
		echo entrega >> ./src/main/java/ayedd/entrega
		git add ./src/main/java/ayedd/entrega
		addall
   		git commit -am "entrega convocatoria extraordinaria: $2"
    		git push origin rama-alumno
		git tag -f extraordinaria
		git push -f origin extraordinaria
		exit 0
	}>>  ./rprueba.output 2>&1
fi

#
# modify pom.xml to compile only code of requested tests
#
sed -i "s#<include>ayedd/\(.*\)/\(.*\)<\/include>#<include>ayedd/$EXERCISE/\2</include>#" pom.xml

#
# strip package line from *.java if it exists and insert the right one
#
for f in src/main/java/ayedd/$EXERCISE/*.java 
do
    sed -i '/package ayedd.*/d' $f
    sed -i "1 i package ayedd.${EXERCISE};" $f
done



{
    echo "done merge: second part"

    addall

    git commit -am "$2"
    git push origin rama-alumno
} >> ./rprueba.output 2>&1

