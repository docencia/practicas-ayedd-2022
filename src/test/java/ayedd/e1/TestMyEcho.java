package ayedd.e1;
import java.time.Instant;
import java.time.Duration;
import edu.princeton.cs.algs4.Out;

import java.util.Random;
import java.util.Arrays;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

import java.io.File;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.*; 
import org.junit.rules.Timeout;

import org.junit.contrib.java.lang.system.ExpectedSystemExit ;

import org.junit.rules.TestWatcher;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;


import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.powermock.api.mockito.PowerMockito.*;



@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ayedd.e1.*")
public class TestMyEcho {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);
 
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    public void ioTest(boolean fast, String comentario, String esperado, String[] args) {
	System.out.println("\n"+ comentario + "\nEsperado:");
	System.out.print(esperado);
	System.out.print("----");
	System.out.println("\nEncontrado:");
	MyEcho.main(args);
	    
	System.out.println("----");
    }

	static public class TestState{
		BufferedWriter grade;
		TestState() {
			try{	
				grade = new BufferedWriter(new FileWriter("./target/surefire-reports/grade.e1", false)); 
				grade.write("@@@CLAUSES (TestBasic min 0 this 0) (TestFunc min -4.0 this -4.0)  (ioTests max 1 this -1)\n");
				grade.write("@@@\tTestBasic\t5\n");
			}catch (IOException e) {
				AssertionError ae = new AssertionError(e);
				throw ae;
			} 
		} 
		public void done() {	
			try{
				grade.close();
			}catch (IOException e) {
				AssertionError ae = new AssertionError(e);
				throw ae;
			} 
		} 
		public void outGrade(String region, double g) {	
			try{
				grade.write("@@@ "+"\t"+region+"\t"+g+"\n");
			}catch (IOException e) {
				AssertionError ae = new AssertionError(e);
				throw ae;
			} 
		} 
	}
	static TestState testSt;
	@BeforeClass
	public static void beforeTest() {
		testSt = new TestState();
	} 
	@AfterClass
	public static void afterTest() {
		testSt.done();
	} 


	@Rule
	public TestRule testWatcher = new TestWatcher() {
	@Override
	public Statement apply(Statement base, Description description) {
		return super.apply(base, description);
	}

	
	@Override
	protected void failed(Throwable e, Description description) {
		switch(description.getMethodName()){
		case "TestEcho_1":
		case "TestEcho_2":
			testSt.outGrade("TestFunc", -1.0);
			break;
		case "TestEcho_0":
			System.out.print("MyEcho no sale con error?");
			testSt.outGrade("TestFunc", -1.0);
		default:
			break;
		}
	}
	};
    @Test
    public void TestEcho_1() {
	String comentario = "Un argumento: MyEcho hola";

	String esperado = "hola\n";
    	String[] args = {"hola"};
	
	ioTest(false, comentario, esperado, args);
    }


    @Test
    public void TestEcho_2() {
	String comentario = "Dos argumentos: MyEcho hola adios";

	String esperado = "hola\tadios\n";
    	String[] args = {"hola", "adios"};
	
	ioTest(false, comentario, esperado, args);
}


    @Test
    public void TestEcho_0() {
	exit.expectSystemExitWithStatus(1);
	String comentario = "Cero argumentos: MyEcho";

	String esperado = "usage: myecho arg...\n";
    	String[] args = {};
	
	ioTest(false, comentario, esperado, args);
    }
}
