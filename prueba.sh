#!/usr/bin/bash

if [ "$USER" = paurea ]; then
	echo  do not run as $USER it is dangerous 1>&2
	exit 1
fi

#
# update local repo
#
{
    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno

    git checkout rama-alumno
    git merge master

    git add 'target/surefire-reports/*.txt'
    git add '*.java'
    git add '*LEEME'
    git add '*.pdf'
    git add '*.txt'
    git add '*.PDF'
    git add '*.TXT'

    
    # rankings
    git add 'time.txt'

    git commit -am "$2"
} > ./prueba.output 2>&1

usage(){
    echo "Tests disponibles:"
    cat tests | sed 's/^/    /'
    echo ""
    echo "Tienes que introducir entrecomillados el nombre de los tests y un comentario:"
    echo "Ejemplo:"
    echo "    ./prueba.sh \"ayedd.e1.TestMyEcho\" \"Arreglado código con argumentos\" "
    exit 1
}

if [[ "$1" = "-h" ]] || [[ $# -ne 2 ]];
then
	usage
fi





#
# prepare tests
#
{
    rm target/surefire-reports/$1-output.txt
    rm src/test/java/ayedd/*/*java
    TEST=`echo $1 | sed  's/\./\//g'`
    echo $TEST
    cp src/test/java/$TEST.java.orig src/test/java/$TEST.java || echo no such test $TEST


} >> ./prueba.output 2>&1

if ! ls src/test/java/*/*/*.orig 2> /dev/null|grep '/'$TEST'\.java\.orig' > /dev/null 2> /dev/null;
then
	echo no existe el test $1 1>&2
	usage
fi

#
# name of the exercise we want to test
#
EXERCISE=`echo $1 | sed "s/ayedd\.\(.*\)\..*/\1/"`

#
# modify pom.xml to compile only code of requested tests
#
sed -i "s#<include>ayedd/\(.*\)/\(.*\)<\/include>#<include>ayedd/$EXERCISE/\2</include>#" pom.xml

#
# strip package line from *.java if it exists and insert the right one
#
for f in src/main/java/ayedd/$EXERCISE/*.java 
do
    sed -i '/package ayedd.*/d' $f
    sed -i "1 i package ayedd.${EXERCISE};" $f
done

#
# do tests
#
mvn -Dmaven.test.failure.ignore=true -Dtest="$1" test | egrep -v "WARNING"

#
# do i/o tests
#
mvn  exec:exec -Dexec.args="target/surefire-reports/$1-output.txt" | egrep -v "WARNING"




