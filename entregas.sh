#!/bin/sh

usage() {	
	echo ./entregas.sh 1>&2
	exit 1
}

nofiles() {	
	echo no hay entregas 1>&2
	exit 1
}

if ! [ -f ./src/main/java/ayedd/e1/LEEME ]; then
	nofiles
fi

if ! ls ./src/main/java/ayedd/[ex]*/*.java > /dev/null 2>&1; then
	nofiles
fi

entregas=no
for i in ./src/main/java/ayedd/[zyex]*/*.java; do
	name=`echo $i | sed -E 's,^\./,,g'`
	#echo git cat-file blob origin/rama-alumno:$name
	if git cat-file blob origin/rama-alumno:$name > /dev/null 2>&1; then
		entregas=yes
		echo -n 'entrega '$i '	'
		#echo git log --invert-grep --author pheras --author paurea --author pedro.delasheras\
		#	--oneline --date=format:'%d-%m-%Y %H:%M:%S' '--format=%h %ae %ad %s' origin/rama-alumno -- $name
		git log --invert-grep --author pheras --author paurea --author pedro.delasheras\
			--oneline --date=format:'%d-%m-%Y %H:%M:%S' '--format=%h %ae %ad %s' origin/rama-alumno -- $name | sed 1q
	fi
done
if [ "$entregas" = no ]; then
	nofiles
fi
